var express = require("express");
var methodOverride = require("method-override");
var app = express();
var bodyparser = require("body-parser");
var mongoose = require("mongoose");
// var expressSanitizer = require("express-sanitizer");

// mongoose.connect("mongodb://localhost/blog_app");
mongoose.connect("mongodb://harsh:harsh@ds229458.mlab.com:29458/blog");
app.set("view engine" , "ejs");
app.use(express.static("public"));
app.use(bodyparser.urlencoded({extended: true}));
// app.use(expressSanitizer());
app.use(methodOverride("_method"));

var blogschema =new mongoose.Schema({
    title:String,
    image:String,
    body:String,
    created:{type:Date, default:Date.now}
});
var blog = mongoose.model("blog", blogschema);
app.get("/", function(req,res){
    res.redirect("/blogs");
});
app.get("/blogs", function(req, res){
    blog.find({}, function(err, blogs){
        if(err){
            console.log("error");
        }else{
            res.render("index",{blogs:blogs});
        }
    });
});
app.get("/blogs/new", function(req, res){
    res.render("new");
});
app.post("/blogs", function(req , res){

   blog.create(req.body.blog, function(err ,newblog){
       if(err){
           console.log("error");
       }else{
            res.redirect("/blogs");
       }
   }); 
});
app.get("/blogs/:id", function(req, res){
    blog.findById(req.params.id, function(err, foundblog)
    {
        if(err){
            console.log("error");
        }else
        {
            res.render("show", {blog: foundblog});
        }
    });
    
});
 app.get("/blogs/:id/edit" , function(req, res){
     blog.findById(req.params.id, function(err, foundBlog)
     {
        if(err){
            console.log("something went wrong");
        } else(
            res.render("edit", {blog:foundBlog})
            );
     });
     
 });
 app.put("/blogs/:id", function(req, res){
    //  req.body.blog.body = req.sanitize(req.body.blog.body);
     blog.findByIdAndUpdate(req.params.id, req.body.blog, function(err, updatedblog){
         if(err){
             console.log("error");
         }else{
             res.redirect("/blogs/" + req.params.id);
         }
     });
 });
 app.delete("/blogs/:id", function(req, res){
    blog.findByIdAndRemove(req.params.id, function(err){
        if(err){
            res.redirect("/blogs");
        }else{
            res.redirect("/blogs");
        }
    })
 });


app.listen(process.env.PORT,process.env.IP , function(){
    console.log("server is running!!");
})